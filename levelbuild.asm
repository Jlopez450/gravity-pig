
build_stage1:
		move.w #$8134,$4(a4)
		lea (music2+40),a2
		move.l a2,vgm_start
		move.w #$0000,scroll_amount
		move.b #$00,gswaps	
		move.b #$00,rasterflag	
		lea (level1tiles),a5
		move.w #$0600,d4
		bsr vram_loop	
		
		lea (palette),a5
		move.l #$c0000000,$4(a4)
		move.w #$003f,d4
		bsr vram_loop		
 		lea (background1), a5
		move.l #$70000000,$4(a4)
		move.w #$1960,d4
		bsr vram_loop
		move.l #$40000003,$4(a4)	;vram write $c000
		move.w #$0DFF,d4		
		lea (bg1_map),a5
		bsr map_background	;note to self: Imagenesis only exports correctly at 512 wide			
		bsr set_hud		
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$00B0,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0090,(a5)+		
		bsr clearcrate
		
		lea (level1),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level1),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision
		
		move.l #$0000a604,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext1),a5
		bsr overlay_text
		move.l #$0000a684,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext2),a5
		bsr overlay_text
		move.l #$0000A704,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext2_2),a5
		bsr overlay_text	
		lea (level1_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text		
		move.w #$8174,$4(a4)		
		rts 
		
build_stage2:
		move.w #$8134,$4(a4)
		move.w #$0000,scroll_amount
		move.b #$02,gswaps	
		move.b #$00,rasterflag				
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4)		
		move.w #$0000,grav_dir		
		lea (sprite_buffer),a5
		move.w #$0110,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0090,(a5)+	
		bsr clearcrate
		
		lea (level2),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level2),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision

		move.l #$0000a308,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext3),a5
		bsr overlay_text
		move.l #$0000a388,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext4),a5
		bsr overlay_text
		lea (level2_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text
		move.w #$8174,$4(a4)		
		rts
		
build_stage3:
		move.w #$8134,$4(a4)
		move.w #$0000,scroll_amount
		move.b #$02,gswaps	
		move.b #$00,rasterflag			
		bsr set_hud			
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4)		
		move.w #$0000,grav_dir		
		lea (sprite_buffer),a5
		move.w #$0110,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0090,(a5)+	

		move.w #$00b0,(a5)+ ;crate
		move.w #$0502,(a5)+
		move.w #$20f0,(a5)+
		move.w #$0100,(a5)+		
		lea (level3),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level3),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision

		move.l #$0000a304,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext5),a5
		bsr overlay_text
		move.l #$0000a384,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext6),a5
		bsr overlay_text
		lea (level3_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text
		move.w #$8174,$4(a4)		
		rts		
		
build_stage4:
		move.w #$8134,$4(a4)
		move.w #$0000,scroll_amount
		move.b #$01,gswaps	
		move.b #$00,rasterflag			
		bsr set_hud			
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4)		
		move.w #$0000,grav_dir		
		lea (sprite_buffer),a5
		move.w #$00A0,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0130,(a5)+
		bsr clearcrate		
	
		lea (level4),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level4),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision
		lea (sprite_buffer+4),a5 
		move.w (a5),d1
		or.w #$0800,d1;face player left
		move.w d1, (a5)	
		
		move.l #$0000A61C,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext7),a5
		bsr overlay_text
		
		move.l #$0000A69C,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext8),a5
		bsr overlay_text
		
		move.l #$0000A71C,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext9),a5
		bsr overlay_text
		lea (level4_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text		
		
		move.w #$8174,$4(a4)		
		rts
 
build_stage5:
		move.w #$8134,$4(a4)
		move.w #$0000,scroll_amount
		move.b #$01,gswaps
		move.b #$00,rasterflag			
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		
		lea (sprite_buffer),a5
		move.w #$0138,(a5)+
		move.w #$0D01,(a5)+	;pig
		move.w #$20E0,(a5)+
		move.w #$0090,(a5)+
		
		move.w #$0138,(a5)+ ;crate
		move.w #$0502,(a5)+
		move.w #$20f0,(a5)+
		move.w #$0100,(a5)+	
		
		lea (level5),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level5),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision
		
		lea (level5_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text	
		
		move.w #$8174,$4(a4)		
		rts 

build_stage6:
		cmpi.b #$FF,skipflag
		beq boxskip1
		move.b #$ff,skipflag
		move.w total_bacon, d0
		move.w stagebacon, d1 ;previous stage
		sub.w d1,d0
		move.w d0,stagebacon
		move.w d0,stage1bacon
		bsr bumperbox1	
boxskip1:	
		move.w #$8134,$4(a4)
		lea (music3+40),a2
		move.l a2,vgm_start
		move.w #$0000,scroll_amount
		move.b #$06,gswaps	
		move.b #$00,rasterflag
		
		move.l #$50000000,$4(a4)
		lea (level2tiles),a5
		move.w #$0600,d4
		bsr vram_loop	
		
		lea (palette2),a5
		move.l #$c0000000,$4(a4)
		move.w #$003f,d4
		bsr vram_loop		
 		lea (background2), a5
		move.l #$70000000,$4(a4)
		move.w #$3EC0,d4
		bsr vram_loop
		move.l #$40000003,$4(a4)	;vram write $c000
		move.w #$0DFF,d4		
		lea (bg2_map),a5
		bsr map_background	;note to self: Imagenesis only exports correctly at 512 wide			
		bsr set_hud		
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$00e0,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0088,(a5)+		
		bsr clearcrate
		
		lea (level6),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level6),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision	
		lea (level6_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text		
		move.w #$8174,$4(a4)		
		rts 
		
build_stage7:
		move.b #$00,skipflag
		move.w #$8134,$4(a4)
		move.w #$0000,scroll_amount
		move.b #$01,gswaps	
		move.b #$00,rasterflag
			
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$00C0,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0080,(a5)+		
		bsr clearcrate
		
		lea (level7),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level7),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision	
		lea (level7_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text		
		move.w #$8174,$4(a4)		
		rts	
		
build_stage8:
		move.w #$8134,$4(a4)
		move.w #$0000,scroll_amount
		move.b #$03,gswaps	
		move.b #$00,rasterflag
			
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$0140,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0088,(a5)+		
		
		move.w #$0098,(a5)+ ;crate
		move.w #$0502,(a5)+
		move.w #$20f0,(a5)+
		move.w #$0200,(a5)+	
		
		lea (level8),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level8),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision	
		lea (level8_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text		
		move.w #$8174,$4(a4)		
		rts

build_stage9:
		cmpi.b #$FF,skipflag
		beq boxskip2
		move.b #$ff,skipflag
		move.w total_bacon, d0
		move.w stagebacon, d1 ;previous stage
		sub.w d1,d0
		move.w d0,stagebacon
		bsr bumperbox2
boxskip2:		
		move.w #$8134,$4(a4)
		lea (music4+40),a2
		move.l a2,vgm_start		
		move.w #$0000,scroll_amount
		move.b #$01,gswaps	
		move.b #$00,rasterflag
		move.b #$ff,colorflag
		move.w #$0CCC,spikecolor
	
		move.l #$50000000,$4(a4)
		lea (level3tiles),a5
		move.w #$0600,d4
		bsr vram_loop	
		
		lea (palette3),a5
		move.l #$c0000000,$4(a4)
		move.w #$003f,d4
		bsr vram_loop		
 		lea (background3), a5
		move.l #$70000000,$4(a4)
		move.w #$1010,d4
		bsr vram_loop
		move.l #$40000003,$4(a4)	;vram write $c000
		move.w #$0DFF,d4		
		lea (bg3_map),a5
		bsr map_background	;note to self: Imagenesis only exports correctly at 512 wide			
		bsr set_hud		
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
	
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$00f2,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0088,(a5)+		
		bsr clearcrate
		
		lea (level9),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level9),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision
		lea (level9_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text	
		move.w #$8174,$4(a4)	
		rts	

build_stage10:
		move.b #$00,skipflag
		move.w #$8134,$4(a4)
		move.w #$0000,scroll_amount
		move.b #$04,gswaps	
		move.b #$00,rasterflag
			
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$0130,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0080,(a5)+		
		
		move.w #$0130,(a5)+ ;crate
		move.w #$0502,(a5)+
		move.w #$20f0,(a5)+
		move.w #$0098,(a5)+	
		
		lea (level10),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level10),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision
		lea (level10_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text		
		move.w #$8174,$4(a4)		
		rts
		
build_stage11:
		move.w #$8134,$4(a4)
		move.w #$0000,scroll_amount
		move.b #$02,gswaps	
		move.b #$00,rasterflag
			
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$0130,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0080,(a5)+	
		
		move.w #$00A0,(a5)+ ;crate
		move.w #$0502,(a5)+
		move.w #$20f0,(a5)+
		move.w #$0208,(a5)+	
		
		lea (level11),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level11),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision	
		lea (level11_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text		
		move.w #$8174,$4(a4)		
		rts	

build_stage12:
		move.w #$8134,$4(a4)
		move.w #$0000,scroll_amount
		move.b #$02,gswaps	
		move.b #$00,rasterflag
			
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$0140,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0080,(a5)+	
		bsr clearcrate
		
		lea (level12),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level12),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision	

		move.l #$0000a382,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext10),a5
		bsr overlay_text
		move.l #$0000a402,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (infotext11),a5
		bsr overlay_text		
		lea (level12_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text		
		move.w #$8174,$4(a4)		
		rts	

build_stage13:
		move.w #$8134,$4(a4)
		move.w #$0000,scroll_amount
		move.b #$99,gswaps	
		move.b #$00,rasterflag
			
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$00E0,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0090,(a5)+	
		bsr clearcrate
		
		lea (levelTBA),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (levelTBA),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision	

		lea (levelTBA_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text
		move.w #$8174,$4(a4)		
		rts	

build_stage14:
		move.w #$8134,$4(a4)
		move.w #$0000,scroll_amount
		move.b #$03,gswaps	
		move.b #$00,rasterflag
			
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$0100,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$00b0,(a5)+	
		bsr clearcrate
		
		lea (level14),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (level14),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision	
		lea (level14_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text
		move.w #$8174,$4(a4)		
		rts		

build_stage15:
		move.w #$8134,$4(a4)
		move.w #$FF90,scroll_amount
		move.b #$01,gswaps	
		move.b #$00,rasterflag
			
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$0120,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0100,(a5)+	
		bsr clearcrate
		
		lea (stage15),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (stage15),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision	

		lea (level15_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text
		move.w #$8174,$4(a4)		
		rts			

build_stage_test:
		move.w #$8134,$4(a4)
		lea (music4+40),a2
		move.l a2,vgm_start
		move.w #$0000,scroll_amount
		move.b #$00,gswaps	
		move.b #$00,rasterflag	
 		lea (background1), a5
		move.l #$70000000,$4(a4)
		move.w #$1960,d4
		bsr vram_loop
		move.l #$40000003,$4(a4)	;vram write $c000
		move.w #$0DFF,d4		
		lea (bg1_map),a5
		bsr map_background	;note to self: Imagenesis only exports correctly at 512 wide			
		bsr set_hud		
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$0120,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$0090,(a5)+		
		bsr clearcrate

		lea (title_level),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (title_level),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision	
		move.w #$8174,$4(a4)		
		rts 
build_title:
		move.w #$8134,$4(a4)
		lea (music4+40),a2
		move.l a2,vgm_start
		move.w #$0000,scroll_amount
		move.b #$00,gswaps	
		move.b #$00,rasterflag		
		move.w #$0000,grav_dir
		
		lea (sprite_buffer),a5
		move.w #$00FE,(a5)+
		move.w #$0D01,(a5)+
		move.w #$38E0,(a5)+
		move.w #$0178,(a5)+		
		bsr clearcrate
		bsr update_sprites
		lea (title_level),a6
		move.l #$40000003,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		move.w #$8174,$4(a4)
		rts 
		
ld_torturestage:
		move.w #$8134,$4(a4)		
		lea (music2+40),a2
		move.l a2,vgm_start
		move.w #$0000,scroll_amount
		move.b #$00,gswaps	
		move.b #$00,rasterflag	
		lea (level1tiles),a5
		move.l #$50000000,$4(a4)
		move.w #$0600,d4
		bsr vram_loop	
		
		lea (palette),a5
		move.l #$c0000000,$4(a4)
		move.w #$003f,d4
		bsr vram_loop		
 		lea (background1), a5
		move.l #$70000000,$4(a4)
		move.w #$1960,d4
		bsr vram_loop
		move.l #$40000003,$4(a4)	;vram write $c000
		move.w #$0DFF,d4		
		lea (bg1_map),a5
		bsr map_background	;note to self: Imagenesis only exports correctly at 512 wide			
		bsr set_hud		
		move.l #$70000003,$4(a4);vram write $f000
		move.w scroll_amount,(a4) ;fixes scroll jump issue		
		move.w #$0000,grav_dir
		lea (sprite_buffer),a5
		move.w #$00B0,(a5)+
		move.w #$0D01,(a5)+
		move.w #$20E0,(a5)+
		move.w #$00F0,(a5)+		
		bsr clearcrate
		
		lea (torturestage),a6
		move.l #$60000002,$4(a4)
		move.w #$00700,d4
		moveq #$00000000,d1
		bsr stage_loop	
		lea (torturestage),a6
		lea (level_buffer),a5
		move.w #$0380,d4
		bsr load_collision	
		lea (torturestage_name),a5
		move.l #$40000003,$4(a4)
		bsr overlay_text		
		move.w #$8174,$4(a4)	
		rts 
		
cutscene:
		cmpi.b #$FF,skipflag
		beq boxskip3
		move.b #$ff,skipflag
		move.w total_bacon, d0
		move.w stagebacon, d1 ;previous stage
		sub.w d1,d0
		move.w stage1bacon, d1 ;previous stage
		sub.w d1,d0		
		move.w d0,stagebacon	
			
		bsr bumperbox3	
boxskip3:	
		move.w #$8134,$4(a4)	
        move.b #$00,colorflag		
		move.b #$01,cutscene_ID
		move.b #$03,vblank_ID
		move.w #$8134,$4(a4)
 		; lea (lego_neutral), a5
		; lea (ram_start), a1
        ; bsr decompress	
		; move.w #$9100,$4(a4)  		;DMA length
		; move.w #$9446,$4(a4) 
		; move.w #$9500,$4(a4)       ;00
		; move.w #$9600,$4(a4)       ;00
		; move.w #$977f,$4(a4)      ;ff
		; move.l #$70000084,$4(a4)		
		
		; lea (ram_start), a5
		; move.w #$4600,d4
		; move.l #$70000000,$4(a4)
		; bsr vram_loop
		
		move.l #$40000003,$4(a4)	
		bsr generate_map
		move.w #$8230,$4(a4)
	    move.l #$70000003,$4(a4);vram write $f000	
		move.w #$0000,(a4)
		move.w #$0000,(a4) 
		
		lea (sprite_buffer),a5
		move.w #$0148,(a5)+
		move.w #$0D01,(a5)+
		move.w #$a0E0,(a5)+
		move.w #$0088,(a5)+
		bsr clearcrate
		bsr update_sprites
		
 		lea (palette_lego_neutral), a5
		move.w #$000f,d4
		move.l #$C0000000, $4(a4)
		bsr vram_loop	
		move.w #$8174,$4(a4)
		lea (music_mystic1+40),a2
		move.l music_mute,vgm_start			
		clr d5
		rts
	
clearcrate:	
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		rts
		