 			  dc.l $FFFFF0, start, BusError, AddressError
		   	  dc.l IllegalInstr, ZeroDivide, ChkInstr, TrapvInstr
		   	  dc.l privilegevio, Trace, Line1010Emu, Line1111Emu
			  dc.l ErrorTrap
	          dc.b "DON'T LOOK AT THIS!", $00
	          dc.l ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l HBlank,    ErrorTrap, VBlank,    ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.b "SEGA MEGASIS    "
              dc.b "(C) 2015 James L"
              dc.b "Gravity Pig                                     "
              dc.b "Gravity Pig                                     "
              dc.b "GM 00000000-05"
game_checksum:			  
              dc.l $69c4d217        ;checksum
              dc.b "J             "
              dc.l 0
              dc.l ROM_End
              dc.l $FF0000
              dc.l $FFFFFF
              dc.b "    "
              dc.b "    "
              dc.b "    "
              dc.b "            "                           
              dc.b "This program contains blast processing! "
              dc.b "JUE REGION FREE!"
              