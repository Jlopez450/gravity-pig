startboss:
		;move.w sr,-(sp)
		move.w #$2700,sr
		move.w #$8134,$4(a4)
		move.w #$8406,$4(a4)
		move.w #$8238,$4(a4)
		;move.w #$8AFF,$4(a4)
		
		move.b #$07,vblank_id
		move.b #$01,removed
		move.b #$01,random
		move.b #$01,spikedir
		move.b #$ff,rasterflag
		move.b #$00,spikeflag		
		move.b #$00,cutscene_ID		
		move.w #$0000,pcm_counter
		move.w #$0000,wallspeed
		move.w #$4100,spikedat
		move.w #$0060,wallpos
		
		move.l #$60000000,$4(a4)
		lea (bossspikes),a5
		move.w #$0300,d4
		bsr vram_loop
		
		move.l #$70000000,$4(a4)
		move.w #$6000,d4
		bsr vram_clear_loop
		
		lea (bossfight_help),a5
		move.l #$46800003,$4(a4)
		bsr smalltextloop
		
		move.w #$8174,$4(a4)		
		bsr bosswait
		
		move.l #$40000003,$4(a4)
		move.w #$0800,d4
		bsr vram_clear_loop		

		move.l #$C0400000,$4(a4)
		lea (palette_bossfight),a5
		move.w #$001f,d4
		bsr vram_loop
		
		lea (sprite_buffer),a5
		move.w #$00F0,(a5)
		add.l #$06,a5
		move.w #$0110,(a5)
		
		bsr set_hud_boss
		move.l #$c0000000,$4(a4)
		lea (palette_boss),a5
		move.w #$000F,d4
		bsr vram_loop
		
		lea (boss_bg),a5
		move.l #$70000000,$4(a4)
		move.w #$4100,d4
		bsr vram_loop
		
		move.l #$41000003,$4(a4)
        move.w #$001A,d5
        move.l #$00000180,d0         ;first tile		
		bsr superloop
		
		lea (music_mystic3)+40,a2
		move.l a2,vgm_start
		lea (music_mystic2)+40,a2	

		move.w #$8174,$4(a4)
		move.w #$2300,sr
		;move.w (sp)+,sr
		;rts
bossloop:
		bsr random_number
		bra bossloop
		
bosswait:
		bsr read_controller
		cmpi.b #$7f,d3
		bne bosswait
		move.w #$8134,$4(a4)		
		rts

final_boss: ;vblank vector
		bsr read_controller
		bsr bosscontrol
		bsr animate_spikes		
		bsr update_sprites
		bsr test_collision
	    bsr draw_hud_boss			
		bsr clock		
		bsr calc_raster
		bsr music_driver
		rte
		
random_number:
		;move.w #$2700,sr
		add.b #$01,random
		cmpi.b #$09,random
		bgt resetrandom
		;move.w #$2300,sr		
		rts
resetrandom:
		move.b #$01,random
		;move.w #$2300,sr		
		rts	
		
bosscontrol:
		move.w #$03,speed
		
		move.b d3,d7
		or.b #$ef,d7
		cmpi.b #$ef, d7
		beq gofast
speedret:		
		move.b d3,d7
		or.b #$fb,d7
		cmpi.b #$fb, d7
		beq bossleft
bosret0:
		move.b d3,d7
		or.b #$f7,d7
		cmpi.b #$f7, d7
		beq bossright
bosret1:		
		move.b d3,d7
		or.b #$fe,d7
		cmpi.b #$fe, d7
		beq bossup
bosret2:		
		move.b d3,d7
		or.b #$fd,d7
		cmpi.b #$fd, d7
		beq bossdown	
		rts
gofast:
		move.w #$0005,speed
		bra speedret
		
bossleft:		
		lea (sprite_buffer+6),a5
		move.w (a5),d1
		cmpi.w #$0080,d1
		ble return
		sub.w speed,d1
		move.w d1,(a5)
		lea (sprite_buffer+4),a5
		move.w (a5),d1
		or.w #$0800,d1
		move.w d1, (a5)
		bsr animate_player
		bra bosret0		
		
bossright:
		lea (sprite_buffer+6),a5
		move.w (a5),d1
		cmpi.w #$01A0,d1
		bge return		
		add.w speed,d1
		move.w d1,(a5)
		lea (sprite_buffer+4),a5
		move.w (a5),d1
		andi.w #$F0FF,d1
		move.w d1, (a5)	
		bsr animate_player		
		bra bosret1			
		
bossup:		
		lea (sprite_buffer),a5
		move.w (a5),d1
		cmpi.w #$0080,d1
		ble return		
		sub.w speed,d1
		move.w d1,(a5)
		bsr animate_player
		bra bosret2

bossdown:
		lea (sprite_buffer),a5
		move.w (a5),d1
		cmpi.w #$0150,d1
		bge return		
		add.w speed,d1
		move.w d1,(a5)
		bsr animate_player		
		rts	
		
resetwall_up:
		move.w #$0200,wallpos
		move.b #$00,hitflag
		clr d6
		clr d5	
		move.b #$01, d5
		move.b levelnum, d6	
		abcd d5, d6
		move.b d6, levelnum 				
		;bsr remove_random	
		move.b random,removed

		clr d6		
		move.b random,d6	
		lsr.b #$01,d6
		move.b d6,spikedir
		bra animate_spikes
resetwall:
		move.w #$0000,wallpos
		move.b #$00,hitflag
		clr d6
		clr d5	
		move.b #$01, d5
		move.b levelnum, d6	
		abcd d5, d6
		move.b d6, levelnum 				
		;bsr remove_random	
		move.b random,removed
		
		clr d6
		move.b random,d6
		lsr.b #$01,d6
		move.b d6,spikedir		
		bra animate_spikes
animate_spikes:	
		cmpi.b #$01,spikedir
		beq animate_spikes_down
		cmpi.b #$02,spikedir
		beq animate_spikes_up
		cmpi.b #$03,spikedir
		beq animate_spikes_right
		cmpi.b #$04,spikedir
		beq animate_spikes_left
		
		sub.b #$01,spikedir
		bra animate_spikes
		;rts

animate_spikes_down:
		lea (sprite_buffer)+88,a5	
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		
		cmpi.w #$0060,wallpos
		bge skip1
		lea (sprite_buffer)+88,a5	
		move.w #$0090,(a5)+
		move.w #$0f00,(a5)+
		move.w #$E120,(a5)+
		move.w #$0110,(a5)+
skip1:		
		move.w #$4100,spikedat
		move.w wallspeed,d6
		add.w d6,wallpos
		cmpi.w #$0200,wallpos
		bge resetwall
		lea (sprite_buffer)+8,a5			
		bra render_spikes_H
animate_spikes_up:
		lea (sprite_buffer)+88,a5	
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		
		cmpi.w #$0180,wallpos
		ble skip2
		lea (sprite_buffer)+88,a5	
		move.w #$0130,(a5)+
		move.w #$0f00,(a5)+
		move.w #$E120,(a5)+
		move.w #$0110,(a5)+
skip2:
		move.w #$5100,spikedat		
		move.w wallspeed,d6
		sub.w d6,wallpos
		cmpi.w #$0010,wallpos
		ble resetwall_up
		lea (sprite_buffer)+8,a5			
		bra render_spikes_H	
animate_spikes_right:
		lea (sprite_buffer)+88,a5	
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		
		cmpi.w #$0060,wallpos
		bge skip3
		lea (sprite_buffer)+88,a5	
		move.w #$00E0,(a5)+
		move.w #$0f00,(a5)+
		move.w #$E120,(a5)+
		move.w #$0090,(a5)+
skip3:	
		move.w #$4110,spikedat	
		move.w wallspeed,d6
		add.w d6,wallpos
		cmpi.w #$0200,wallpos
		bge resetwall
		lea (sprite_buffer)+8,a5			
		bra render_spikes_V	

animate_spikes_left:
		lea (sprite_buffer)+88,a5	
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		move.w #$0000,(a5)+
		
		cmpi.w #$01C0,wallpos
		ble skip4
		lea (sprite_buffer)+88,a5	
		move.w #$00E0,(a5)+
		move.w #$0f00,(a5)+
		move.w #$E120,(a5)+
		move.w #$0190,(a5)+
skip4:	
		move.w #$4910,spikedat	
		move.w wallspeed,d6
		sub.w d6,wallpos
		cmpi.w #$0010,wallpos
		ble resetwall_up
		lea (sprite_buffer)+8,a5			
		bra render_spikes_v
	
					
render_spikes_H:		
		move.w wallpos,(a5)+
		move.w #$0f02,(a5)+
		move.w spikedat,(a5)+
		move.w #$0080,(a5)+	
		move.w wallpos,(a5)+
		move.w #$0f03,(a5)+
		move.w spikedat,(a5)+
		move.w #$00A0,(a5)+		
		move.w wallpos,(a5)+
		move.w #$0f04,(a5)+
		move.w spikedat,(a5)+
		move.w #$00c0,(a5)+	
		move.w wallpos,(a5)+
		move.w #$0f05,(a5)+
		move.w spikedat,(a5)+
		move.w #$00e0,(a5)+	
		move.w wallpos,(a5)+
		move.w #$0f06,(a5)+
		move.w spikedat,(a5)+
		move.w #$0100,(a5)+	
		move.w wallpos,(a5)+
		move.w #$0f07,(a5)+
		move.w spikedat,(a5)+
		move.w #$0120,(a5)+		
		move.w wallpos,(a5)+
		move.w #$0f08,(a5)+
		move.w spikedat,(a5)+
		move.w #$0140,(a5)+
		move.w wallpos,(a5)+
		move.w #$0f09,(a5)+
		move.w spikedat,(a5)+
		move.w #$0160,(a5)+	
		move.w wallpos,(a5)+
		move.w #$0f0a,(a5)+
		move.w spikedat,(a5)+
		move.w #$0180,(a5)+
		move.w wallpos,(a5)+
		move.w #$0f0b,(a5)+
		move.w spikedat,(a5)+
		move.w #$01a0,(a5)+
		
		bra remove_spike
		
render_spikes_V: ;bugged (steals a life for no reason)
		move.w #$0080,(a5)+		
		move.w #$0f02,(a5)+
		move.w spikedat,(a5)+
		move.w wallpos,(a5)+	
		move.w #$00A0,(a5)+		
		move.w #$0f03,(a5)+
		move.w spikedat,(a5)+
		move.w wallpos,(a5)+
		move.w #$00C0,(a5)+		
		move.w #$0f04,(a5)+
		move.w spikedat,(a5)+
		move.w wallpos,(a5)+
		move.w #$00E0,(a5)+		
		move.w #$0f05,(a5)+
		move.w spikedat,(a5)+
		move.w wallpos,(a5)+
		move.w #$0100,(a5)+		
		move.w #$0f06,(a5)+
		move.w spikedat,(a5)+
		move.w wallpos,(a5)+
		move.w #$0120,(a5)+		
		move.w #$0f07,(a5)+
		move.w spikedat,(a5)+
		move.w wallpos,(a5)+
		move.w #$0140,(a5)+		
		move.w #$0f08,(a5)+
		move.w spikedat,(a5)+
		move.w wallpos,(a5)+
		move.w #$0160,(a5)+		
		move.w #$0f09,(a5)+
		move.w spikedat,(a5)+
		move.w wallpos,(a5)+
		
		move.w #$0180,(a5)+		
		move.w #$0f0a,(a5)+
		move.w spikedat,(a5)+
		move.w wallpos,(a5)+
		move.w #$01a0,(a5)+		
		move.w #$0f0b,(a5)+
		move.w spikedat,(a5)+
		move.w wallpos,(a5)+
	
		cmpi.b #$07,removed
		bge bandaid
		bra remove_spike		
bandaid:
		sub.b #$03, removed
remove_spike:		
		clr d6
		move.b removed,d6
		lea (removetable),a0
        lsl.b #$1,d6		    ;locate the correct table
		sub.w #$02,d6           ;step back a word 
		add.w d6,a0             ;adjust the address register 
		move.w (a0),d6
		move.l d6,a0            ;switch to direct addressing 	
		jmp (a0)
		
removetable:
 dc.w remove1
 dc.w remove2
 dc.w remove3
 dc.w remove4
 dc.w remove5
 dc.w remove6
 dc.w remove7
 dc.w remove8
 dc.w remove9
 dc.w remove9 ;in case of accidental overflow :)
 dc.w remove9
 
remove1:
		lea (sprite_buffer)+8,a5
		move.w #$0000,(a5)
		lea (sprite_buffer)+16,a5
		move.w #$0000,(a5)		
		rts
remove2:
		lea (sprite_buffer)+16,a5
		move.w #$0000,(a5)
		lea (sprite_buffer)+24,a5
		move.w #$0000,(a5)		
		rts		
remove3:	
		lea (sprite_buffer)+24,a5
		move.w #$0000,(a5)
		lea (sprite_buffer)+32,a5
		move.w #$0000,(a5)		
		rts	
remove4:
		lea (sprite_buffer)+32,a5
		move.w #$0000,(a5)
		lea (sprite_buffer)+40,a5
		move.w #$0000,(a5)		
		rts		
remove5:
		lea (sprite_buffer)+40,a5
		move.w #$0000,(a5)
		lea (sprite_buffer)+48,a5
		move.w #$0000,(a5)		
		rts		
remove6:	
		lea (sprite_buffer)+48,a5
		move.w #$0000,(a5)
		lea (sprite_buffer)+56,a5
		move.w #$0000,(a5)		
		rts		
remove7:
		lea (sprite_buffer)+56,a5
		move.w #$0000,(a5)
		lea (sprite_buffer)+64,a5
		move.w #$0000,(a5)		
		rts				
remove8:
		lea (sprite_buffer)+64,a5
		move.w #$0000,(a5)
		lea (sprite_buffer)+72,a5
		move.w #$0000,(a5)		
		rts				
remove9:
		lea (sprite_buffer)+72,a5
		move.w #$0000,(a5)
		lea (sprite_buffer)+80,a5
		move.w #$0000,(a5)		
		rts			
		
test_collision:		
		clr d6
		move.w $4(a4),d6
		andi.w #$0020,d6
		tst d6
		bne collide
		rts			
collide:
		tst hitflag
		 bne return		 
		cmpi.b #$00,lives
		 beq game_over
		 
		clr d0
		clr d4
		move.b lives,d0
		move.b #$01,d4
		sbcd d4,d0	
		move.b d0,lives	
		move.b #$ff,hitflag
		rts	
		
set_hud_boss:
		move.l #$0000C000,d0
		bsr calc_vram
		move.l d0,$4(a4)
		lea (hud),a6
		move.w #$0072,d4
		clr d1
text_loop_boss:	
		move.b (a6)+,d1
		;andi.w #$00ff,d1
		;eor.w #$4000,d1
        move.w d1,(a4)	
		dbf d4, text_loop_boss
		rts	
draw_hud_boss:	
		move.l #$0000c08c,d0
		bsr calc_vram
		move.l d0,$4(a4)		
        clr d0
		move.b minutes, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0
			
		move.w d0, (a4)
		;clr d0
		move.b minutes, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
			
		move.w d0, (a4)	

		move.l #$0000c092,d0
		bsr calc_vram
		move.l d0,$4(a4)		
		move.b seconds, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0
			
		move.w d0, (a4)
		move.b seconds, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
			
		move.w d0, (a4)	

		move.l #$0000c0a4,d0
		bsr calc_vram
		move.l d0,$4(a4)	
		move.b lives, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)
		
		move.b lives, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)

		move.l #$0000c0b6,d0
		bsr calc_vram
		move.l d0,$4(a4)	
		move.b level_number, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)
		move.b level_number, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)

		move.l #$0000c0ca,d0
		bsr calc_vram
		move.l d0,$4(a4)	
		move.b gswaps, d0
		andi.b #$f0, d0
		lsr.b #$04, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)
		move.b gswaps, d0
		andi.b #$0f, d0
		add.b #$30,d0
		andi.w #$00ff,d0
		move.w d0, (a4)			
		rts	
wingame:
		move.l music_mute,vgm_start
		lea (music_ending+40),a2
		bsr endcutscene
		move.w #$2700,sr
		move.b #$00,spikedir		
		move.w #$8AFF,$4(a4)		
		move.l #$70000000,$4(a4)
		move.w #$6600,d4
        bsr vram_clear_loop	
		lea (palette)+2,a5
		move.w #$40,d4
		move.l #$c0000000,$4(a4)
		bsr vram_loop
		move.l #$c0000000,$4(a4)
		move.w #$0e80,(a4)
		move.l #$c0420000,$4(a4)
		move.w #$0e80,(a4)		
		move.w #$0000,(a4)		
		move.w #$0000,(a4)		
		move.b #$08,vblank_ID
		move.b #$00,rasterflag
		lea (ending_text),a5
		move.l #$0000C000,d1
		move.l #$40000003,$4(a4)		
		move.w #$0028,d4		
		clr d5
		move.w #$2300,sr
		bra loop
		