cutsceneloop:
		bsr cutscene_input
		bsr new_cutscene
		lea (cutsceneprompt),a5
		move.l #$4d140003,$c00004	;vram CD14
		bsr overlay_text
		move.l #$70000003,$C00004
		move.w #$0000,(a4) ;bandaid for the screen scroll issue
		move.w #$0000,(a4)
		bsr music_driver
		rte
		
cutscene_input:
		or.b #$Bf,d3
		cmpi.b #$Bf,d3
		 beq no_input	;fixes the flip spam issue
		bsr read_controller
		move.b d3,d7
		or.b #$Bf,d7
		cmpi.b #$BF, d7
		beq advance_cutscene	
		rts
advance_cutscene:
		add.b #$01,cutscene_ID
		rts
		
new_cutscene:
		move.b oldcutscene_ID,d5
		cmp.b cutscene_ID,d5
		beq return
		move.b cutscene_ID,oldcutscene_ID
		lea (cutscenes),a0
		move.l #$00000000,d0
		move.b cutscene_ID,d0
        lsl.b #$1,d0		    ;locate the correct table
		sub.w #$02,d0           ;step back a word 
		add.w d0,a0             ;adjust the address register 
		move.w (a0),d0
		move.l d0,a0            ;switch to direct addressing 
		jmp (a0)

cutscenes:	
 dc.w cutscene1		
 dc.w cutscene2		
 dc.w cutscene3	
 dc.w boss
 ;dc.w cutscenex		
		
		
cutscene1:	
		move.w #$8134,$C00004
 		lea (lego_neutral), a5
		
		; move.w #$4600,d4
		; move.l #$70000000,$c00004
		; bsr vram_loop

		lea (ram_start), a1
        bsr decompress			
		move.w #$9100,$c00004  		;DMA length
		move.w #$9446,$c00004 
		move.w #$9500,$c00004       ;00
		move.w #$9600,$c00004       ;00
		move.w #$977f,$c00004      ;ff
		move.l #$70000084,$c00004
		
		lea (dialogue1),a5
		move.l #$0000c102,d5
		bsr draw_dialogue

		lea (dialogue2),a5
		move.l #$0000cb02,d5
		bsr draw_dialogue	
		move.w #$8174,$C00004		
		rts
		
cutscene2:	
		move.w #$8134,$C00004	
		move.l #$0000c100,d0
		bsr calc_vram
		move.l d0,$c00004
		
		lea (dialogue3),a5
		move.l #$0000c100,d5
		bsr draw_dialogue	

		lea (dialogue4),a5
		move.l #$0000cb02,d5
		bsr draw_dialogue		
		
		move.w #$8174,$C00004		
		rts	
cutscene3:	
		move.w #$8134,$C00004
		
		move.l #$40000003,$C00004	;vram write $c000
		
		bsr generate_map		
 		lea (palette_lego_smile), a5
		move.w #$000f,d4
		move.l #$C0000000, $c00004
		bsr vram_loop
	
 		lea (lego_smile), a5
		move.w #$4600,d4
		move.l #$70000000,$c00004
		bsr vram_loop		
		
		lea (dialogue5),a5
		move.l #$0000c102,d5
		bsr draw_dialogue		
		move.w #$8174,$C00004			
		rts
		
endcutscene:	
		move.w #$8134,$C00004
		
		lea (sprite_buffer),a5
		move.w #$0148,(a5)+
		move.w #$0D01,(a5)+
		move.w #$a0E0,(a5)+
		move.w #$0088,(a5)+
		bsr clearcrate
		bsr update_sprites
		
	    move.l #$70000003,$4(a4);vram  
	    move.w #$0000,(a4)
	    move.w #$0000,(a4)
		
		lea (palette),a5
		move.w #$3f,d4
		move.l #$c0000000,$C00004
		bsr vram_loop
		
		move.l #$40000003,$C00004	;vram write $c000	
		bsr generate_map		
 		lea (palette_lego_angry), a5
		move.w #$000f,d4
		move.l #$C0000000, $c00004
		bsr vram_loop	
 		lea (lego_angry), a5
		move.w #$4600,d4
		move.l #$70000000,$c00004
		bsr vram_loop
		lea (dialogue6),a5
		move.l #$0000c102,d5
		bsr draw_dialogue
		lea (cutsceneprompt),a5
		move.l #$4d120003,$c00004	;vram CD12
		bsr overlay_text		
		move.w #$8174,$C00004
endwait1:
		bsr read_controller
		cmpi.b #$bf,d3
		bne endwait1
		bsr unhold
		move.w total_bacon,d7
		bsr hex2dec
		cmpi.w #$0130,d7
		blt bad_ending
		
		lea (dialogue8),a5
		move.l #$0000c102,d5
		bsr draw_dialogue
		
		sub.w #$0130,d7
		
		move.l #$41820003,$4(a4) ;vram C182		
		move.w d7,d0
		andi.w #$0F00,d0
		lsr.w #$08,d0
		eor.w #$c000,d0
		add.w #$0030,d0		
		move.w d0,(a4)
		
		move.w d7,d0
		andi.w #$00F0,d0
		lsr.w #$04,d0
		eor.w #$c000,d0
		add.w #$0030,d0		
		move.w d0,(a4)	

		move.w d7,d0
		andi.w #$000F,d0
		eor.w #$c000,d0
		add.w #$0030,d0
		move.w d0,(a4)			
endwait2:	
		bsr read_controller	
		cmpi.b #$bf,d3
		bne endwait2
		bsr unhold		
		rts

		
bad_ending:		
		move.b #$ff,bad_end_flag
		lea (dialogue7),a5
		move.l #$0000c102,d5
		bsr draw_dialogue
		move.l #$41A40003,$4(a4) ;vram C1A4
		
		move.w d7,d0
		andi.w #$0F00,d0
		lsr.w #$08,d0
		eor.w #$c000,d0
		add.w #$0030,d0		
		move.w d0,(a4)
		
		move.w d7,d0
		andi.w #$00F0,d0
		lsr.w #$04,d0
		eor.w #$c000,d0
		add.w #$0030,d0		
		move.w d0,(a4)	

		move.w d7,d0
		andi.w #$000F,d0
		eor.w #$c000,d0
		add.w #$0030,d0
		move.w d0,(a4)			
		
endwait3:	
		bsr read_controller	
		cmpi.b #$bf,d3
		bne endwait3
		bsr unhold		
		bsr setup_vdp		
		bsr ld_torturestage
		move.b #$02,vblank_ID		
		move.w #$2300,sr
		bra loop
		
cutsceneX:	
		move.w #$8134,$C00004
		move.l #$40000003,$C00004	;vram write $c000	
		bsr generate_map		
 		lea (palette_lego_angry), a5
		move.w #$000f,d4
		move.l #$C0000000, $c00004
		bsr vram_loop	
 		lea (lego_angry), a5
		move.w #$4600,d4
		move.l #$70000000,$c00004
		bsr vram_loop	
		
		move.w #$8174,$C00004			
		rts	

boss:
		bra startboss
		